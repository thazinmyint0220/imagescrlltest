import { EntrantRecord } from "./EntrantRecord";

export class EntrantRecordList {
  private _entrantRecordList: EntrantRecord[];

  constructor(entrantRecordList: EntrantRecord[] = []) {
    this._entrantRecordList = entrantRecordList;
  }

  get entrantRecordList(): EntrantRecord[] {
    return this._entrantRecordList;
  }

  set entrantRecordList(value: EntrantRecord[]) {
    this._entrantRecordList = value;
  }

  // Additional methods for manipulating the list can be added here if necessary
  addEntrantRecord(entrantRecord: EntrantRecord): void {
    this._entrantRecordList.push(entrantRecord);
  }

  removeEntrantRecord(index: number): void {
    if (index >= 0 && index < this._entrantRecordList.length) {
      this._entrantRecordList.splice(index, 1);
    }
  }
}
